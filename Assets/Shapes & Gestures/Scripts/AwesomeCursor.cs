﻿using UnityEngine;
using System.Collections;

public class AwesomeCursor : MonoBehaviour
{
    public GameObject trailObject;
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    
    void Start()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }

    void Update()
    {
        trailObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));

        if (Input.GetMouseButton(0) || Input.touchCount > 0)
            trailObject.SetActive(true);
        else
            trailObject.SetActive(false);
    }
}
