﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(PointCloudRegognizer))]
public class Game : MonoBehaviour
{
    public RendererTemplates RendererTemplatePrefab;
    public Text TimeText,
                CurrScoreText,
                ScoreText;

    public GameObject TrailCursor,
                      Renderer,
                      Panel,
                      Over;

    private PointCloudRegognizer regognizer;
    private RendererTemplates rendererTemplate;
    private Transform templateRoot;
    private int currentTemplateID;
    private int currentScore = 0;
    private int startTime = 10;
    private float currentTime;

    void Start()
    {
        //textUI = CanvasGameObject.transform.GetChild(1).GetChild(0).GetComponent<Text>();

        currentTime = (float)startTime;

        templateRoot = new GameObject("Gesture Templates").transform;
        templateRoot.parent = this.transform;
        templateRoot.localScale = 8f * Vector3.one;

        regognizer = GetComponent<PointCloudRegognizer>();

        rendererTemplate = Instantiate(RendererTemplatePrefab, templateRoot.position, templateRoot.rotation) as RendererTemplates;
        rendererTemplate.transform.parent = templateRoot;
        rendererTemplate.transform.localPosition = Vector3.zero;
        rendererTemplate.transform.localScale = Vector3.one;
        NextTemplate();
    }

    void FixedUpdate()
    {
        currentTime -= Time.deltaTime;
        TimeText.text = "Осталось: " + currentTime.ToString("0.00") + "с.";

        if (currentTime < 0f)
        {
            GameOver();
        }
    }

    void OnCustomGesture(PointCloudGesture gesture)
    {
        RendererTemplates rt = FindGestureRenderer(gesture.RecognizedTemplate);
        if (!rt) return;

        currentScore++;
        CurrScoreText.text = "Счёт: " + currentScore;

        NextTemplate();
        rt.Blink();
        currentTime = (float)--startTime;
    }

    RendererTemplates FindGestureRenderer(PointCloudGestureTemplate template)
    {
        return template == rendererTemplate.GestureTemplate ? rendererTemplate : null;
    }

    void NextTemplate()
    {
        if (currentTemplateID > regognizer.Templates.Count - 1) return;
        rendererTemplate.GestureTemplate = regognizer.Templates[currentTemplateID];
        rendererTemplate.name = regognizer.Templates[currentTemplateID].name;
        currentTemplateID++;
    }

    void GameOver()
    {
        ScoreText.text = "Ваш счёт: " + currentScore;
        Over.SetActive(true);
        Panel.SetActive(false);
        TrailCursor.SetActive(false);
        Renderer.SetActive(false);
        
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
